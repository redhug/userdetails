/** 
*  User model
*  Describes the characteristics of each attribute in a customer resource.
*
* @author Pavan Kumar Reddy Byreddy <S534625@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  name:{ type: String, required: true},
  email: { type: String, required: true, unique: true },
  phone: { type: String, required: true, unique: true }
})

module.exports = mongoose.model('Customer', CustomerSchema)
