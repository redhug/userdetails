/** 
*  Bank Transaction model
*  Describes the characteristics of each attribute in a Bank Transaction - one entry for one transaction.
*
* @author Leela Krishna<S534629@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const BankTransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accountNumber: { type: Number, required: true },
  accountName: { type: String, required: true },
  accountType: {type: String, required: true },
  transactionType: { type: String, required: true },
  date: { type: Date, required: true },
  TransactionAmount: {type: Number, required: true}
})

module.exports = mongoose.model('BankTransaction', BankTransactionSchema)
