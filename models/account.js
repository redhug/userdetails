/** 
*  Order model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Anvesh Rokanlawar
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  accountNumber: { type: Number, required: true, unique: true, default: 555 },
  accountBalance:{ type:Number,required:true,unique:false,default:0},
  accountCreated: { type: String, required: true, default: 'yyyy/mm/dd'},
  accountType: { type: String, enum: ['checking', 'savings', 'current'], required: true, default: 'NA'}
})

module.exports = mongoose.model('account', AccountSchema)