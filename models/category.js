/** 
*  Category model
*  Describes the characteristics of each attribute in a Category resource.
*
* @author VenkataSandeep K <S534965@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  account_category: { type: String, required: true, unique: true },
  debit_cardnumber: {type: String, required: true, unique: true},
  
  credit_cardNumber: {type: String, required: true, unique: true},
  description: { type: String, required: false, default: 'description' },
})

module.exports = mongoose.model('categories', CategorySchema)
