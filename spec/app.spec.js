// Jasmine provides a function called "describe"
// arg1 - is the description
// arg2 - is the function (the logic)

var request = require('request')

const base_url = "https://localhost:8080"

describe('Test app.js', () => {
    describe('GET /', () => {
        it('returns a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200)
            })
        })
    })
    describe('GET /about', () => {
        it('returns a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200)
            })
        })
    })
    describe('GET /user', () => {
        it('returns a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200)
            })
        })
    })
    describe('GET /transaction', () => {
        it('returns a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200)
            })
        })
    })
    describe('GET /category', () => {
        it('returns a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200)
            })
        })
    })
    describe('GET /account', () => {
        it('returns a status code of 200', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(200)
            })
        })
    })
    describe('GET /badlink', () => {
        it('returns a status code of 404', () => {
            request.get(base_url, (error, response, body) => {
                expect(response.statusCode).toBe(404)
            })
        })
    })
})