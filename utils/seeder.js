// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file

const users = require('../data/user.json')
const account = require('../data/account.json')
const transactions = require('../data/transaction.json')
const category = require('../data/category.json')

// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  // Customers don't depend on anything else...................

  db.users = new Datastore()
  db.users.loadDatabase()

  // insert the sample data into our data store
   db.users.insert(users)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.users = db.users.find(users)
  LOG.debug(`${app.locals.users.query.length} users seeded`)
  //console.log('${app.locals.users.query.length} users seeded')
  // Products don't depend on anything else .....................

  db.accounts = new Datastore()
  db.accounts.loadDatabase()

  
  db.accounts.insert(account)

 
  app.locals.accounts = db.accounts.find(account)
  LOG.debug(`${app.locals.accounts.query.length} account seeded`)
  //console.log('${app.locals.users.query.length} users seeded')

  
  db.transactions = new Datastore()
  db.transactions.loadDatabase()

  
  db.transactions.insert(transactions)

 
  app.locals.transactions = db.transactions.find(transactions)
  LOG.debug(`${app.locals.transactions.query.length} transactions seeded`)
  //console.log('${app.locals.transactions.query.length} transactions seeded')
  

  db.categories = new Datastore()
  db.categories.loadDatabase()
															  

  
  db.categories.insert(category)

  
  app.locals.categories = db.categories.find(category)
  LOG.debug(`${app.locals.categories.query.length} category seeded`)

  LOG.info('END Seeder. Sample data read and verified.')
}